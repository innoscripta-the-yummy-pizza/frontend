// Global imports
import React from 'react';
import DocumentTitle from 'react-document-title';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ReactRouterGlobalHistory } from 'react-router-global-history';
import './App.scss';

// Import Components
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

// Import pages
import Home from './modules/general/Home/Home';
import Login from './modules/general/Login/Login';
import Menu from './modules/general/Menu/Menu';
import Register from './modules/general/Register/Register';
import ChangePassword from './modules/customer/ChangePassword/ChangePassword';
import ForgetPassword from './modules/customer/ForgetPassword/ForgetPassword';
import Orders from './modules/customer/Orders/Orders';
import Profile from './modules/customer/Profile/Profile';
import VerifyAccount from './modules/customer/VerifyAccount/VerifyAccount';

// Admin pages
import AdminLogin from './modules/admin/AdminLogin/AdminLogin';
import AdminOrders from './modules/admin/AdminOrders/AdminOrders';

class App extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showShadow: false,
    };

    this.navToShadow = this.navToShadow.bind(this);
  }

  navToShadow = (e) => {
    this.setState({ showShadow: e.mode === 'leave' });
  }

  render() {
    const { showShadow } = this.state;
    return ([
      <Router>
        <ReactRouterGlobalHistory />
        <Header key="header" className={showShadow ? 'show-shadow' : ''} />
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/menu" component={Menu} />
          <Route path="/change-password" component={ChangePassword} />
          <Route path="/forget-password" component={ForgetPassword} />
          <Route path="/orders" component={Orders} />
          <Route path="/profile" component={Profile} />
          <Route path="/verify-account" component={VerifyAccount} />
          <Route path="/admin/orders" component={AdminOrders} />
          <Route path="/admin" component={AdminLogin} />
          <Route path="/" component={Home} />
        </Switch>
        <Footer key="footer" />
      </Router>,
      <DocumentTitle title="The Yummy Pizza" />,
    ]);
  }
}

export default App;
