import axios from 'axios';
import getHistory from 'react-router-global-history';
import { notification } from 'antd';

const generalHeaders = {
  Accept: 'application/json',
};

function handleUnauthorized(response) {
  if (response.status === 403) {
    window.localStorage.removeItem('access_token');
    notification.error({
      message: response?.data.message,
      description: 'This user is not allowed to perform this action. Please login first.',
    });
    const history = getHistory();
    if (history) history.push('/login');
  }
}

export const get = async (endpoint) => {
  const config = { headers: { ...generalHeaders } };
  return axios
    .get(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
};

export async function post(endpoint, data = {}) {
  const config = { headers: { ...generalHeaders } };
  return axios
    .post(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, data, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}

export async function remove(endpoint) {
  const config = { headers: { ...generalHeaders } };
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}

export async function update(endpoint, data = {}) {
  const config = { headers: { ...generalHeaders } };
  return axios
    .put(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, data, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}

export async function authGet(endpoint) {
  const accessToken = window.localStorage.getItem('access_token');
  const config = { headers: { ...generalHeaders, Authorization: `Bearer ${accessToken}` } };
  return axios
    .get(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}

export async function authPost(endpoint, data = {}) {
  const accessToken = window.localStorage.getItem('access_token');
  const config = { headers: { ...generalHeaders, Authorization: `Bearer ${accessToken}` } };
  return axios
    .post(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, data, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}

export async function authRemove(endpoint) {
  const accessToken = window.localStorage.getItem('access_token');
  const config = { headers: { ...generalHeaders, Authorization: `Bearer ${accessToken}` } };
  return axios
    .delete(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}

export async function authUpdate(endpoint, data = {}) {
  const accessToken = window.localStorage.getItem('access_token');
  const config = { headers: { ...generalHeaders, Authorization: `Bearer ${accessToken}` } };
  return axios
    .put(`${process.env.REACT_APP_API_URL}/api/v1${endpoint}`, data, config)
    .catch((error) => handleUnauthorized(error.response))
    .then((response) => response?.data)
    .then((response) => response?.data);
}
