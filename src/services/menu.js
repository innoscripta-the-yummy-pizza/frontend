import { get, authGet } from './api';

export const retrievePizzas = async () => get('/menu');
export const singlePizza = async (id) => authGet(`/menu/${id}`);
export const retrieveExtras = async () => authGet('/menu/extras');
