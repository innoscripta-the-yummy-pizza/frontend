import React from 'react';
import PropTypes from 'prop-types';
import QueueAnim from 'rc-queue-anim';
import { Element } from 'rc-scroll-anim';
import { Link } from 'react-router-dom';
import pizzaGif from '../../static/images/pizza.gif';

class Banner extends React.PureComponent {
  render() {
    const {
      className,
      isMobile,
      navToShadow,
      title,
      image,
      content,
      extras,
    } = this.props;
    return (
      <Element component="section" className={`${className}-wrapper page`} onChange={navToShadow}>
        <div className={className}>
          <div className={`${className}-img-wrapper`}>
            <img src={image} alt={title} />
          </div>
          <QueueAnim
            type={isMobile ? 'bottom' : 'right'}
            className={`${className}-text-wrapper`}
            delay={300}
          >
            <h1 key="h1">{title}</h1>
            <p className="main-info" key="p">{content}</p>
            {extras}
          </QueueAnim>
        </div>
      </Element>
    );
  }
}

Banner.defaultProps = {
  className: 'banner',
  isMobile: false,
  navToShadow: () => {},
  title: 'The Yummy Pizza',
  image: pizzaGif,
  content: 'Where pizza is not only our food, is a piece of our lifes.',
  extras: <Link className="ant-btn ant-btn-primary" to="/register">Get Ready</Link>,
};

Banner.propTypes = {
  className: PropTypes.string,
  isMobile: PropTypes.bool,
  navToShadow: PropTypes.func,
  title: PropTypes.string,
  image: PropTypes.string,
  content: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  extras: PropTypes.array,
};

export default Banner;
