/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../static/images/main-logo.png';

export default function Header(props) {
  return (
    <header {...props}>
      <Link className="logo-wrapper" to="/">
        <img src={logo} alt="The Yummy Pizza" className="main-logo" />
        <span>The Yummy Pizza</span>
      </Link>
      <div className="button">
        <Link className="custom-link" to="/menu">Menu</Link>
        <Link className="custom-link" to="/login">Login</Link>
        <Link className="custom-link" to="/register">Register</Link>
      </div>
    </header>
  );
}
