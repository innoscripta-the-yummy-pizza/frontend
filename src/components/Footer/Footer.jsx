import React from 'react';
import { Row, Col } from 'antd';

function Footer() {
  return (
    <footer id="footer" className="dark">
      <Row className="bottom-bar">
        <Col lg={6} sm={24} />
        <Col lg={18} sm={24}>
          <span style={{ marginRight: 12 }}>Moisés Benrenguer</span>
          <span style={{ marginRight: 12 }}>Copyright © Al rights reserved</span>
        </Col>
      </Row>
    </footer>
  );
}

export default Footer;
