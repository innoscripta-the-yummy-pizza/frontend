import React from 'react';
import { enquireScreen } from 'enquire-js';

import Banner from '../../../components/Banner/Banner';
import Section1 from './Section1';
import Section2 from './Section2';
import Section3 from './Section3';
import Section4 from './Section4';
import '../../../static/style';

let isMobileView = false;
enquireScreen((b) => {
  isMobileView = b;
});

class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: isMobileView,
    };
  }

  componentDidMount() {
    enquireScreen((b) => {
      this.setState({
        isMobile: !!b,
      });
    });
  }

  render() {
    const { isMobile } = this.state;
    return (
      [
        <Banner key="banner" isMobile={isMobile} navToShadow={this.navToShadow} />,
        <Section1 key="section1" />,
        <Section2 key="section2" />,
        <Section3 key="section3" />,
        <Section4 key="section4" />,
      ]
    );
  }
}

export default Home;
