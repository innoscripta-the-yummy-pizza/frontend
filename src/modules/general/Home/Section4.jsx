import React from 'react';
import PropTypes from 'prop-types';
import { Carousel } from 'antd';
import QueueAnim from 'rc-queue-anim';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';
import { page4 } from './data';

export default function Section4(props) {
  const children = page4.map((item, i) => (
    <QueueAnim type="bottom" key={i.toString()}>
      <img key="user" className="user" src={item.avatar} alt="" />
      <div key="comment" className="comment">
        {!props.isMobile && <img src="" alt="" />}
        <span>{item.comment}</span>
      </div>
      <h4 key="name">{item.name}</h4>
      <p key="pro">{item.profile}</p>
    </QueueAnim>
  ));
  const { isMobile } = props;
  return (
    <OverPack component="section" className="page-wrapper page4">
      <QueueAnim
        type="bottom"
        className="page text-center"
        leaveReverse
        key="a"
      >
        <div key="1" className="carousel-wrapper">
          <Carousel effect={isMobile ? 'scrollx' : 'fade'}>
            {children}
          </Carousel>
        </div>
      </QueueAnim>
    </OverPack>
  );
}

Section4.defaultProps = {
  isMobile: PropTypes.bool,
};

Section4.propTypes = {
  isMobile: PropTypes.bool,
};
