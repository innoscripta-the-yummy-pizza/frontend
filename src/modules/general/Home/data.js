import camionIcon from '../../../static/images/camion.png';
import pizzaIcon from '../../../static/images/pizza.png';
import dineroIcon from '../../../static/images/dinero.png';

import baconCorn from '../../../static/images/pizzas/BaconCorn.jpg';
import chickenMustard from '../../../static/images/pizzas/ChickenAndMustardHoney.jpg';
import mexican from '../../../static/images/pizzas/Mexican.jpg';

export const page1 = [
  {
    img: camionIcon,
    title: 'Fast Delivery',
    description: 'Super fast delivery times, 15mins to have it in hoe is more than enough.',
  },
  {
    img: pizzaIcon,
    title: 'Yummy Pizza',
    description: 'Variety in different country styles pizzas.',
  },
  {
    img: dineroIcon,
    title: 'Pay and go',
    description: 'Online payments enabled. No need for cash usage.',
  },
];

export const page3 = [
  {
    img: baconCorn,
    title: 'Bacon Corn',
    description: 'Multiple flavours.',
  },
  {
    img: chickenMustard,
    title: 'Chicken Mustard',
    description: 'For kids.',
  },
  {
    img: mexican,
    title: 'Mexican',
    description: 'Spicy and delicious',
  },
];

export const page4 = [
  {
    name: 'Shane Wood',
    profile: 'Customer',
    avatar: 'https://os.alipayobjects.com/rmsportal/CcFeLxXurbQmwrT.jpg',
    comment: 'Amazing and fast experience with The Yummy Pizza.',
  },
  {
    name: 'Rike Ariand',
    profile: 'Customer',
    avatar: 'https://zos.alipayobjects.com/rmsportal/wtkIALmYDSmOIiAalkdv.jpg',
    comment: 'Pizza, just another concept.',
  },
];

export const page5 = [

];
