import React from 'react';
import QueueAnim from 'rc-queue-anim';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

export default function Section2() {
  return (
    <OverPack component="section" className="page-wrapper page2">
      <QueueAnim
        type="bottom"
        className="page text-center"
        leaveReverse
        key="page"
      >
        <h2 key="title">Yummy, So Yummy</h2>
        <span key="line" className="separator" />
        <QueueAnim type="bottom" className="info-content" key="content">
          <p className="main-info" key="1">We commit ourselves to yummy excellency</p>
          <p className="main-info" key="2">Every pizza is made thinking on happiness of the people. We make pizza a family time</p>
        </QueueAnim>
      </QueueAnim>
    </OverPack>
  );
}
