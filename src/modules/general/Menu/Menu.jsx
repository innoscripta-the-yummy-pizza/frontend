/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable camelcase */
import React from 'react';
import { enquireScreen } from 'enquire-js';
import {
  Form,
  Col,
  Row,
  Modal,
  Switch,
  Button,
  InputNumber
} from 'antd';
import QueueAnim from 'rc-queue-anim';
import { retrievePizzas, singlePizza, retrieveExtras } from '../../../services/menu';
import Banner from '../../../components/Banner/Banner';
import recursive from '../../../static/images/recursive.gif';

let isMobileView = false;
enquireScreen((b) => {
  isMobileView = b;
});

const layout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
};

class Menu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      pizza: null,
      pizzas: [],
      isMobile: isMobileView,
      modalVisible: false,
      extras: [],
      totalPrice: 0,
      currentQuantity: 1,
    };

    // Adding functions to the current scope
    this.addPizzaToMyOrder = this.addPizzaToMyOrder.bind(this);
    this.confirmPizzaAdd = this.confirmPizzaAdd.bind(this);
    this.discardPizzaAdd = this.discardPizzaAdd.bind(this);
    this.toggleExtra = this.toggleExtra.bind(this);
    this.changeQuantity = this.changeQuantity.bind(this);
  }

  componentDidMount() {
    retrievePizzas()
      .then((pizzas) => this.setState({ pizzas }));

    enquireScreen((b) => {
      this.setState({
        isMobile: !!b,
      });
    });
  }

  // eslint-disable-next-line class-methods-use-this
  addPizzaToMyOrder(pizza) {
    this.setState({
      pizza: null,
      totalPrice: 0,
      modalVisible: false,
      extras: [],
    });

    singlePizza(pizza.id)
      .then((response) => {
        if (!response) return;
        this.setState({
          pizza: response,
          totalPrice: response.price,
          modalVisible: true,
          extras: [],
        });
        retrieveExtras()
          .then((extras) => this.setState({ extras }));
      });
  }

  confirmPizzaAdd() {
    this.setState({ pizza: null, modalVisible: false });
  }

  discardPizzaAdd() {
    this.setState({ pizza: null, modalVisible: false });
  }

  toggleExtra(extra, enabled) {
    const { totalPrice, currentQuantity } = this.state;
    if (enabled) {
      this.setState({ totalPrice: totalPrice + extra.price * currentQuantity });
    } else {
      this.setState({ totalPrice: totalPrice - extra.price * currentQuantity });
    }
  }

  // eslint-disable-next-line class-methods-use-this
  changeQuantity(quantity) {
    const { currentQuantity, totalPrice } = this.state;
    this.setState({
      totalPrice: (totalPrice / currentQuantity) * quantity,
      currentQuantity: quantity,
    });
  }

  render() {
    const {
      pizzas,
      isMobile,
      modalVisible,
      pizza,
      extras,
      totalPrice,
    } = this.state;
    return ([
      <Banner key="banner" isMobile={isMobile} navToShadow={this.navToShadow} image={recursive} title="Menu" content="Here you can see all our specialties. Dive into the pizza." extras={[]} />,
      <section className="page-wrapper page1">
        <QueueAnim component={Row} type="bottom" className="page row text-center" delay={500}>
          {pizzas.map((element) => (
            <Col className="card-wrapper card-pizza" key={`pizza-${element.id}`} md={8} xs={24}>
              <div className="card">
                <img src={element.image_url} alt={element.name} className="card-img-top" />
                <div className="descriptor">
                  <div className="content">
                    <h3>{element.name}</h3>
                    <span className="description text-secondary">{element.description}</span>
                    <br />
                    <span className="description text-secondary">{`${element.price}$`}</span>
                  </div>
                  <button type="button" className="ant-btn ant-btn-primary" onClick={() => { this.addPizzaToMyOrder(element); }}>Add to my order</button>
                </div>
              </div>
            </Col>
          ))}
        </QueueAnim>
      </section>,
      <Modal
        title={pizza?.name}
        visible={modalVisible}
        onOk={this.confirmPizzaAdd}
        onCancel={this.discardPizzaAdd}
        className="pizza-modal"
        footer={[
          <Button key="back" onClick={this.discardPizzaAdd}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={this.confirmPizzaAdd}>
            {`$${totalPrice.toFixed(2)} - Add to my order`}
          </Button>,
        ]}
      >
        <img src={pizza?.image_url} alt={pizza?.name} />
        <div className="description">
          <p>{pizza?.description}</p>
          <p><b>{`${pizza?.price}$`}</b></p>
        </div>
        <hr />
        <Row>
          <Form {...layout} name="basic" className="extras-form">
            {extras.map((extra) => (
              <Form.Item label={extra.name}>
                <small>{`(+${extra.price}$)`}</small>
                <Switch onClick={(checked) => this.toggleExtra(extra, checked)} />
              </Form.Item>
            ))}
          </Form>
        </Row>
        <hr />
        <Row>
          <Form {...layout} name="basic">
            <Form.Item label="Quantity" name="quantiy">
              <InputNumber min={1} max={10} defaultValue={1} onChange={this.changeQuantity} />
            </Form.Item>
          </Form>
        </Row>
      </Modal>
    ]);
  }
}

export default Menu;
