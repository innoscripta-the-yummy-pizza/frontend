/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
  Form,
  Input,
  Button,
  Col,
  Row,
} from 'antd';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onFinish = this.onFinish.bind(this);
    this.onFinishFailed = this.onFinishFailed.bind(this);
  }

  // eslint-disable-next-line class-methods-use-this
  onFinish(values) {
    console.log('Success:', values);
  }

  // eslint-disable-next-line class-methods-use-this
  onFinishFailed(errorInfo) {
    // DO SOMETHING
  }

  render() {
    return (
      <div className="upper-padding down-padding page-height">
        <Row>
          <Col span={12} push={6}>
            <Form {...layout} name="basic" initialValues={{ remember: true, }} onFinish={this.onFinish} onFinishFailed={this.onFinishFailed}>
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  {
                    required: true,
                    message: 'Please input your email!',
                    type: 'email',
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                  Login
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Login;
